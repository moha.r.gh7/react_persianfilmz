class PersianFilmz {
  movies() {
    let data = [
      { id: 1, name: "فیلم" },
      { id: 2, name: "سریال" },
      { id: 3, name: "چهره های مشهور" },
      { id: 4, name: "مراسم و رخدادها" },
      { id: 5, name: "ارتباط با ما" },
      { id: 6, name: "درباره ما" },
    ];
    return data;
  }
}
export default PersianFilmz;
